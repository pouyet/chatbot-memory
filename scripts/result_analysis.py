from collections import defaultdict, namedtuple
from glob import glob

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

trainlogs, testlogs = {}, {} 
rls, smts = [], [] 
models = defaultdict(list) 
for filename in glob("*.txt"): 
    with open(filename, "r") as f: 
        model_params = filename.split("_")[1].split(".")[0] 
        model_lambdas = filename.split("_")[0].split("-")[-2:] 
        model_memory = filename.split("-")[0] 
        model_name = f"{model_memory}_{model_lambdas}_{model_params}" 
        models[model_memory].append(model_name) 
        if model_lambdas[0] != "0.0": 
            rls.append(model_name) 
        else: 
            smts.append(model_name) 
        trainlogs[model_name] = eval(f.readline()) 
        testlogs[model_name] = eval(f.readline())


# There's different strategies to retrieve the actual scores
# Here the best score is computed
# Using last score is probably better
model_scores = {} 
for model, model_names in models.items(): 
    mean_rewards = {} 
    for k in model_names: 
        if k not in rls: 
            continue 
        # This is the line to change to have a different strategie
        # It may be te["reward"][-1] to take the score from the last training epoch
        # or something like the max over the mean of the columns
        max_r_te = np.array([te["reward"] for te in testlogs[k].values()]).max(1) 
        mean_rewards[k] = max_r_te.mean() 
    a = np.array([k for k in mean_rewards.values()]) 
    model_scores[model] = a 

Model = namedtuple("Model", ("rl", "ent", "batch", "nlayers", "hsize", "score"))
model_tuples = {}
for m, names in models.items(): 
    if m == "bc": 
        continue 
    temp = [] 
    print(len(model_scores[m]), len(names)) 
    i = 0 
    for name in names: 
        if name in rls: 
            model_params = name.split("_")[-1].split("-")
            rl, ent = eval(name.split("_")[1])  #map(float, name.split("_")[1].replace("'", "").replace("[", "").replace("]", "").split(","))
            score = model_scores[m][i] 
            temp.append(Model(rl, ent, *model_params, score)) 
            i += 1 
    model_tuples[m] = temp 

dfs = {m: pd.DataFrame(data=tups) for m, tups in model_tuples.items()}