from glob import glob
import matplotlib.pyplot as plt

dirname = "../res/gridsearch_combined"

trainlogs, testlogs = {}, {}
for filename in glob(dirname + "/*"):
    with open(filename, "r") as f:
        print(filename)
        model_params = filename.split("_")[1].split(".")[0]
        model_lambdas = filename.split("_")[0].split("-")[-2:]
        model_name = f"{model_lambdas}_{model_params}"
        print(model_name)
        trainlogs[model_name] = eval(f.readline())
        testlogs[model_name] = eval(f.readline())

for model, testlog in trainlogs.items():
    plt.figure()
    plt.suptitle(model)
    for log_name, log_values in testlog.items():
        plt.plot(log_values, label=log_name)
    plt.legend()
    plt.show()